package com.example.b10102033_hwk4_1;


import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	ImageView[] playerChosen = new ImageView[6]; //玩家選擇的骰子
	ImageView[] randomDice = new ImageView[6]; //電腦隨雞的骰子
	ImageView[] starIV = new ImageView[6]; //六個星星,代表猜中幾個
	int[] diceCounter = new int[6];// 骰子的點數
	int[] iRand = new int[6];
	int right = 0;//答對個數
	TextView tv;
	
	private Handler handler = new Handler(){

        public void handleMessage(Message msg) {
        	super.handleMessage(msg);        	
        	
        	//收到動畫停止訊息後隨機骰子點數
        	for(int i = 0; i < 6; i++){
        		iRand[i] = (int)(Math.random()*6 + 1);
        		randomSwitch(iRand[i], i);
        	}
        	
        	//計算猜對幾個
        	for(int i = 0; i < 6; i++){
    			if(diceCounter[i] == iRand[i]) right++;
    		}
        	
        	for(int i = 0 ;i < right; i++){
        		//答對幾個就亮幾個燈
    			starIV[i].setImageDrawable(getResources().getDrawable(R.drawable.star));
    		}
        }
    };
    
    //取得隨機骰子點數來setImage
    public void randomSwitch(int iRand, int index){
    	switch (iRand) {
		case 1:
			randomDice[index].setImageResource(R.drawable.dice01);
			break;
		case 2:
			randomDice[index].setImageResource(R.drawable.dice02);
			break;
		case 3:
			randomDice[index].setImageResource(R.drawable.dice03);
			break;
		case 4:
			randomDice[index].setImageResource(R.drawable.dice04);
			break;
		case 5:
			randomDice[index].setImageResource(R.drawable.dice05);
			break;
		case 6:
			randomDice[index].setImageResource(R.drawable.dice06);
			break;
		}
    }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		for(int i = 0; i < 6; i++){
			playerChosen[i] = new ImageView(this);
			randomDice[i] = new ImageView(this);
			starIV[i] = new ImageView(this);
			diceCounter[i] = 1;
		}
		
		//取得各個view
		playerChosen[0] = (ImageView) findViewById(R.id.imageView1);
		playerChosen[1] = (ImageView) findViewById(R.id.imageView2);
		playerChosen[2] = (ImageView) findViewById(R.id.imageView3);
		playerChosen[3] = (ImageView) findViewById(R.id.imageView4);
		playerChosen[4] = (ImageView) findViewById(R.id.imageView5);
		playerChosen[5] = (ImageView) findViewById(R.id.imageView6);
		randomDice[0] = (ImageView) findViewById(R.id.imageView7);
		randomDice[1] = (ImageView) findViewById(R.id.imageView8);
		randomDice[2] = (ImageView) findViewById(R.id.imageView9);
		randomDice[3] = (ImageView) findViewById(R.id.imageView10);
		randomDice[4] = (ImageView) findViewById(R.id.imageView11);
		randomDice[5] = (ImageView) findViewById(R.id.imageView12);
		starIV[0] = (ImageView) findViewById(R.id.starImageView1);
		starIV[1] = (ImageView) findViewById(R.id.starImageView2);
		starIV[2] = (ImageView) findViewById(R.id.starImageView3);
		starIV[3] = (ImageView) findViewById(R.id.starImageView4);
		starIV[4] = (ImageView) findViewById(R.id.starImageView5);
		starIV[5] = (ImageView) findViewById(R.id.starImageView6);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//選擇骰子點數
	public void choose(int index){
		switch(diceCounter[index]){
			//點數1
			case 0:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice01));
				diceCounter[index]++;
				break;
			//點數2
			case 1:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice02));
				diceCounter[index]++;
				break;
			//點數3
			case 2:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice03));
				diceCounter[index]++;
				break;
			//點數4
			case 3:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice04));
				diceCounter[index]++;
				break;
			//點數5
			case 4:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice05));
				diceCounter[index]++;
				break;
			//點數6
			case 5:
				playerChosen[index].setImageDrawable(getResources().getDrawable(R.drawable.dice06));
				diceCounter[index] = 0; // 6點的下一個回到1點
				break;
		}
	}
	
	public void playerChoose(View view){
		int id = view.getId(); //取得目前點的imageview id
		
		//點擊第1~6個骰子
		switch(id){
			case R.id.imageView1:
				choose(0);
				break;
			case R.id.imageView2:
				choose(1);
				break;
			case R.id.imageView3:
				choose(2);
				break;
			case R.id.imageView4:
				choose(3);
				break;
			case R.id.imageView5:
				choose(4);
				break;
			case R.id.imageView6:
				choose(5);
				break;
		}
	}
	
	public void BtClick(View view){
		right = 0; //每次rolling之前猜對的要先歸零
		for(int i = 0 ;i < 6; i++){
			//先把所有星星燈改成沒亮
			starIV[i].setImageDrawable(getResources().getDrawable(R.drawable.star2));
		}
		
		//取得動畫xml檔
		Resources res = getResources();
		final AnimationDrawable animDraw = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		final AnimationDrawable animDraw2 = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		final AnimationDrawable animDraw3 = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		final AnimationDrawable animDraw4 = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		final AnimationDrawable animDraw5 = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		final AnimationDrawable animDraw6 = (AnimationDrawable) res.getDrawable(R.drawable.an_drawable);
		randomDice[0].setImageDrawable(animDraw);
		randomDice[1].setImageDrawable(animDraw2);
		randomDice[2].setImageDrawable(animDraw3);
		randomDice[3].setImageDrawable(animDraw4);
		randomDice[4].setImageDrawable(animDraw5);
		randomDice[5].setImageDrawable(animDraw6);
		
		
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//開始骰骰子
					animDraw.start();
					animDraw2.start();
					animDraw3.start();
					animDraw4.start();
					animDraw5.start();
					animDraw6.start();
					
					//動畫跑兩秒
					Thread.sleep(2000);
					
					//動畫停止
					animDraw.stop();
					animDraw2.stop();
					animDraw3.stop();
					animDraw4.stop();
					animDraw5.stop();
					animDraw6.stop();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				//丟訊息給handler
				handler.sendMessage(handler.obtainMessage());
			}
		}
		).start();
	}
}
