package myproject.library;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class choice extends Activity {
	 WebView mWebView;
	 private Spinner spinner;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choice);
        spinner = (Spinner)findViewById(R.id.school_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.schools,
				android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        ImageButton bt_choice = (ImageButton) findViewById(R.id.choice_button);
        bt_choice.setOnClickListener(listener_choice);
        /*Button btn_chu = (Button) findViewById(R.id.button_chu);
        btn_chu.setOnClickListener(listener_chu); 
        Button btn_nthu = (Button) findViewById(R.id.button_nthu);
        btn_nthu.setOnClickListener(listener_nthu); 
        Button btn_nctu = (Button) findViewById(R.id.button_nctu);
        btn_nctu.setOnClickListener(listener_nctu); 
        Button btn_ncku = (Button) findViewById(R.id.button_ncku);
        btn_ncku.setOnClickListener(listener_ncku); 
        Button btn_ncu = (Button) findViewById(R.id.button_ncu);
        btn_ncu.setOnClickListener(listener_ncu); */
    }
    private OnClickListener listener_choice = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	String school = spinner.getSelectedItem().toString();
        	if(school.equalsIgnoreCase("中華大學")){
        		Intent intent =new Intent();
    			intent.setClass(choice.this,search.class);
    			startActivity(intent);
        	}
        	else if(school.equalsIgnoreCase("清華大學")){
        		Uri uri=Uri.parse("http://www.lib.nthu.edu.tw/"); 
            	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
    			startActivity(intent);
        	}
        	else if(school.equalsIgnoreCase("交通大學")){
        		Uri uri=Uri.parse("http://www.lib.nctu.edu.tw/"); 
            	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
    			startActivity(intent);
        	}
        	else if(school.equalsIgnoreCase("成功大學")){
        		Uri uri=Uri.parse("http://www.lib.ncku.edu.tw/www2008/"); 
            	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
    			startActivity(intent);
        	}
        	else {
        		Uri uri=Uri.parse("http://www.lib.ncu.edu.tw/"); 
            	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
    			startActivity(intent);
        	}
        }
        
    };  
   /* private OnClickListener listener_chu = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Intent intent =new Intent();
			intent.setClass(choice.this,search.class);
			startActivity(intent);
        }
        
    };  
    private OnClickListener listener_nthu = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Uri uri=Uri.parse("http://www.lib.nthu.edu.tw/"); 
        	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
			startActivity(intent);
        }
        
    };  
    
    private OnClickListener listener_nctu = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Uri uri=Uri.parse("http://www.lib.nctu.edu.tw/"); 
        	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
			startActivity(intent);
        }
        
    };  
    private OnClickListener listener_ncku = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Uri uri=Uri.parse("http://www.lib.ncku.edu.tw/www2008/"); 
        	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
			startActivity(intent);
        }
        
    };  
    
    private OnClickListener listener_ncu = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Uri uri=Uri.parse("http://www.lib.ncu.edu.tw/"); 
        	Intent intent = new Intent(Intent.ACTION_VIEW,uri); ;
			startActivity(intent);
        }
        
    };*/
}


