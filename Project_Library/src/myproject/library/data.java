package myproject.library;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class data extends Activity {
   final String Insert = "Insert", Search = "Search", List = "List";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.data);
    new AccessDBTask().execute(List); 
    /*Button btnInsert = (Button) findViewById(R.id.btn_insert);
    btnInsert.setOnClickListener(insertListener);*/
    /*Button btnSearch = (Button) findViewById(R.id.btn_search);
    btnSearch.setOnClickListener(searchListener);*/
    /*Button btnList = (Button) findViewById(R.id.btn_list);
    btnList.setOnClickListener(listListener);*/
  }
 
  /*View.OnClickListener insertListener =
    new View.OnClickListener() {
    public void onClick(View v) {
	EditText edtID = (EditText) findViewById(R.id.txtID);
	EditText edtName = (EditText) findViewById(R.id.txtName);
	EditText edtGrade = (EditText) findViewById(R.id.txtGrade);
	String bookID = edtID.getText().toString();
	String bookName = edtName.getText().toString();
	String bookGrade = edtGrade.getText().toString();
	new AccessDBTask().execute(Insert, bookID, bookName, bookGrade); 
    }
  };*/
		  
 /* View.OnClickListener searchListener =
     new View.OnClickListener() {
       public void onClick(View v) {
    	 EditText edtID = (EditText) findViewById(R.id.txtID);
    	 new AccessDBTask().execute(Search, edtID.getText().toString()); 
       }
  };*/
  
 /*View.OnClickListener listListener =
    new View.OnClickListener() {
	  public void onClick(View v) {
		new AccessDBTask().execute(List); 
	  }
  };
		*/  
  private class AccessDBTask extends AsyncTask<String, String, String> 
  { 

    @Override
    protected String doInBackground(String... args) 
    {
      return accessDatabase(args);
    }
    
	@Override
    protected void onPostExecute(String str)
	{
      TextView resultTV = (TextView) findViewById(R.id.lblOutput);
      if (resultTV != null && str != null) resultTV.setText(str);
    }
  }
  
  public String accessDatabase(String... inputStrings) {
	String result = "", resultStr = "";
	//the year data to send
    ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
    
      nameValuePairs.add(new BasicNameValuePair("bookID", ""));
      nameValuePairs.add(new BasicNameValuePair("bookName", ""));   
      nameValuePairs.add(new BasicNameValuePair("bookGrade", "0"));   
      
     /* if(inputStrings[0].equalsIgnoreCase(Insert)) {
    	nameValuePairs.add(new BasicNameValuePair("operation", Insert));
        nameValuePairs.add(new BasicNameValuePair("bookID", inputStrings[1]));
        nameValuePairs.add(new BasicNameValuePair("bookName", inputStrings[2]));
        nameValuePairs.add(new BasicNameValuePair("bookGrade", inputStrings[3]));
      }*/
      if(inputStrings[0].equalsIgnoreCase(Search)) {
    	nameValuePairs.add(new BasicNameValuePair("operation", Search));  
        nameValuePairs.add(new BasicNameValuePair("bookName", inputStrings[1]));
      }
      if(inputStrings[0].equalsIgnoreCase(List))
    	nameValuePairs.add(new BasicNameValuePair("operation", List));  
      
    InputStream is = null;
	     
	//http post
	try {
	  HttpClient httpclient = new DefaultHttpClient();
	  HttpPost httppost = new HttpPost("http://library.csie.chu.edu.tw/library.php");
	  //HttpPost httppost = new HttpPost("http://192.168.0.184/app/library.php");//自架serve
	  httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	  HttpResponse response = httpclient.execute(httppost);
	  HttpEntity entity = response.getEntity();
	  is = entity.getContent();
	} catch(Exception e){
	  Log.e("log_tag", "Error in http connection "+e.toString());
	}
	//convert response to string
	try {
	  BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
	  StringBuilder sb = new StringBuilder();
	  String line = null;
	  while ((line = reader.readLine()) != null) {
	    sb.append(line + "\n");
	  }
	  is.close();     
	  result = sb.toString();	  
	} catch(Exception e){
	  Log.e("log_tag", "Error converting result " + e.toString());
	}
	     
	//parse json data
	try {
	  JSONArray jArray = new JSONArray(result);
	  for (int i = 0; i < jArray.length(); i++) {
	    JSONObject json_data = jArray.getJSONObject(i);
	    resultStr += json_data.getString("bookID")+
	          ", " + json_data.getString("bookName") +
	          ", " + json_data.getDouble("bookGrade") + "\n";
	  }
	} catch(JSONException e){
	  Log.e("log_tag", "Error parsing data "+e.toString());
	}
	return resultStr;
  }
  }