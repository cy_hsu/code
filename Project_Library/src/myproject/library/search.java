package myproject.library;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.SharedPreferences;  
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

//import myproject.library.data.AccessDBTask;
















import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class search extends Activity {
	//AlertDialog.Builder list;
	private Bitmap myBitmap, tempBitmap, tempBitmap2, tempBitmap3;
	private ImageView myImageView;
	private Paint myPaint, myPaint2;
	private Canvas tempCanvas, tempCanvas2, tempCanvas3;
	private int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	int bookcase;
	int total_data = 0 , index = 0; // 目前資料筆數
	//String total_data1;
	
	EditText item;
	private SharedPreferences settings; // 你輸入的喜歡類別
	private static final String data = "DATA";
	private static final String[] like = {"like1", "like2"};
	
	final String Insert = "Insert", Search = "Search", List = "List";
	private static String DATABASE_TABLE = "Library";
	private SQLiteDatabase db;
    
    
    int[] ibuttons={R.id.imageButton1};
    
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private NotificationManager gNotMgr = null;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        TextView ver_view = (TextView)findViewById(R.id.vertial_textview);   
        ver_view.setMovementMethod(ScrollingMovementMethod.getInstance());
        
        item = (EditText) findViewById(R.id.your_like);
        settings = getSharedPreferences(data , 0);
        
        gNotMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        RegistControls(this);
        gNotMgr.cancelAll();
        
        ImageButton btn_list = (ImageButton) findViewById(R.id.button_determine);
        btn_list.setOnClickListener(listener_list);
        ImageButton btn_list1 = (ImageButton) findViewById(R.id.imageButton2);
        btn_list1.setOnClickListener(listener_list1);
        
        myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.lib);
        myImageView = (ImageView) findViewById (R.id.imageButton1);
        
        myPaint = new Paint();
        myPaint.setColor(Color.RED);
        myPaint2 = new Paint();
        myPaint2.setColor(Color.RED);
        
        tempBitmap = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(),
        		Bitmap.Config.RGB_565);
        tempBitmap2 = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(),
        		Bitmap.Config.ARGB_4444);
        tempBitmap3 = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(),
        		Bitmap.Config.ARGB_4444);
        
        tempCanvas2 = new Canvas(tempBitmap2);
        tempCanvas3 = new Canvas(tempBitmap3);
    }
    private OnClickListener listener_list1 = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	Intent intent =new Intent();
  			intent.setClass(search.this,data.class);
  			startActivity(intent);
        }
    };

    
    private OnClickListener listener_online = new View.OnClickListener() {
        public void onClick(View v) {       	
        	Intent intent = new Intent(Intent.ACTION_VIEW, 
   			     Uri.parse("http://lib.chu.edu.tw/spydus/wsearch.htm"));
			startActivity(intent);		
        }
    };  
    private OnClickListener listener_list = new View.OnClickListener() 
    {
        public void onClick(View v) 
        {
           	 EditText edtID = (EditText) findViewById(R.id.edt_bookName);
           	 new AccessDBTask().execute(Search, edtID.getText().toString());
        }
    };
    
    private void RegistControls(final Context context)// 發出通知的函式
    {
    	ImageButton btn_list = (ImageButton) findViewById(R.id.button_determine);
    	final int compare_data; // 比較與現在之資料筆數
    	compare_data = total_data;
   	 
    	btn_list.setOnClickListener(new View.OnClickListener() 
   	 {			
   			@SuppressWarnings("deprecation")
   			@Override
   			public void onClick(View v) 
   			{		
   				EditText edtID = (EditText) findViewById(R.id.edt_bookName);
   	           	new AccessDBTask().execute(Search, edtID.getText().toString()); 
   	           	
   				final String ACTION = "com.android.broadcast.RECEIVER_ACTION";
   				Intent intent = new Intent();
   				intent.setAction(ACTION);
   				sendBroadcast(intent);// 發出通知
   			}
   	}
   	 ); 
    }
    
   
    private class AccessDBTask extends AsyncTask<String, String, String> 
    { 

      @Override
      protected String doInBackground(String... args1) 
      {
        return accessDatabase(args1);
      }
      @Override
     protected void onPostExecute(String str)
  	  {
        TextView resultTV = (TextView) findViewById(R.id.vertial_textview);
        String[] strArray = str.split(" , ");//字串切割
        total_data = Integer.parseInt(strArray[0]);
        
        String numgrade = "";
        for (String d : strArray) 
        {
        	numgrade += d + "\n";
        }        
        
        bookcase = Integer.parseInt(strArray[1]);
        
        tempCanvas = new Canvas(tempBitmap);
        tempCanvas.drawBitmap(myBitmap, 0, 0, null);
        
        tempBitmap2.eraseColor(0x00000000);
        
        switch(bookcase)
        {
        	case 1:
        		x1 = 50; y1 = 1120; x2 = 145; y2 = 1136;
        		break;
        	case 2:
        		x1 = 50; y1 = 1085; x2 = 145; y2 = 1104;
        		break;
        	case 3:
        		x1 = 50; y1 = 1055; x2 = 145; y2 = 1072;
        		break;
        	case 4:
        		x1 = 50; y1 = 1030; x2 = 145; y2 = 1040;
        		break;
        	case 5:
        		x1 = 50; y1 = 995; x2 = 145; y2 = 1008;
        		break;
        	case 6:
        		x1 = 50; y1 = 965; x2 = 145; y2 = 976;
        		break;
    		case 7:
    			x1 = 50; y1 = 930; x2 = 145; y2 = 944;
    			break;
        	case 8:
        		x1 = 50; y1 = 895; x2 = 145; y2 = 912;
        		break;
        	case 9:
        		x1 = 50; y1 = 860; x2 = 145; y2 = 880;
        		break;
        	case 10:
        		x1 = 55; y1 = 460; x2 = 150; y2 = 475;
        		break;
        	case 11:
        		x1 = 55; y1 = 425; x2 = 150; y2 = 443;
        		break;
        	case 12:
        		x1 = 55; y1 = 390; x2 = 150; y2 = 411;
        		break;
        	case 13:
        		x1 = 55; y1 = 355; x2 = 150; y2 = 379;
        		break;
        	case 14:
        		x1 = 55; y1 = 320; x2 = 150; y2 = 347;
        		break;
        	case 15:
        		x1 = 55; y1 = 285; x2 = 150; y2 = 315;
        		break;
        	case 16:
        		x1 = 55; y1 = 250; x2 = 150; y2 = 283;
        		break;
        	case 17:
        		x1 = 55; y1 = 215; x2 = 150; y2 = 251;
        		break;
        	case 18:
        		x1 = 55; y1 = 180; x2 = 150; y2 = 219;
        		break;
        	case 19:
        		x1 = 55; y1 = 145; x2 = 150; y2 = 187;
        		break;
        	case 20:
        		x1 = 55; y1 = 115; x2 = 150; y2 = 155;
        		break;
        	case 21:
        		x1 = 205; y1 = 350; x2 = 225; y2 = 450;
        		break;
        	case 22:
        		x1 = 237; y1 = 350; x2 = 257; y2 = 450;
        		break;
        	case 23:
        		x1 = 269; y1 = 350; x2 = 289; y2 = 450;
        		break;
        	case 24:
        		x1 = 301; y1 = 350; x2 = 321; y2 = 450;
        		break;
        	case 25:
        		x1 = 333; y1 = 350; x2 = 353; y2 = 450;
        		break;
        	case 26:
        		x1 = 365; y1 = 350; x2 = 385; y2 = 450;
        		break;
        	case 27:
        		x1 = 397; y1 = 350; x2 = 417; y2 = 450;
        		break;
        	case 28:
        		x1 = 429; y1 = 350; x2 = 449; y2 = 450;
        		break;
        	case 29:
        		x1 = 461; y1 = 350; x2 = 481; y2 = 450;
        		break;
        	case 30:
        		x1 = 493; y1 = 350; x2 = 513; y2 = 450;
        		break;
        	case 31:
        		x1 = 525; y1 = 350; x2 = 545; y2 = 450;
        		break;
        	case 32:
        		x1 = 557; y1 = 350; x2 = 577; y2 = 450;
        		break;
        	case 33:
        		x1 = 589; y1 = 350; x2 = 609; y2 = 450;
        		break;
        	case 34:
        		x1 = 205; y1 = 220; x2 = 225; y2 = 320;
        		break;
        	case 35:
        		x1 = 237; y1 = 220; x2 = 257; y2 = 320;
        		break;
        	case 36:
        		x1 = 269; y1 = 220; x2 = 289; y2 = 320;
        		break;
        	case 37:
        		x1 = 301; y1 = 220; x2 = 321; y2 = 320;
        		break;
        	case 38:
        		x1 = 333; y1 = 220; x2 = 353; y2 = 320;
        		break;
        	case 39:
        		x1 = 365; y1 = 220; x2 = 385; y2 = 320;
        		break;
        	case 40:
        		x1 = 397; y1 = 220; x2 = 417; y2 = 320;
        		break;
        	case 41:
        		x1 = 429; y1 = 220; x2 = 449; y2 = 320;
        		break;
        	case 42:
        		x1 = 461; y1 = 220; x2 = 481; y2 = 320;
        		break;
        	case 43:
        		x1 = 493; y1 = 220; x2 = 513; y2 = 320;
        		break;
        	case 44:
        		x1 = 525; y1 = 220; x2 = 545; y2 = 320;
        		break;
        	case 45:
        		x1 = 557; y1 = 220; x2 = 577; y2 = 320;
        		break;
        	case 46:
        		x1 = 589; y1 = 220; x2 = 609; y2 = 320;
        		break;
        	case 47:
        		x1 = 650; y1 = 115; x2 = 745; y2 = 135;
        		break;
        	case 48:
        		x1 = 650; y1 = 149; x2 = 745; y2 = 169;
        		break;
        	case 49:
        		x1 = 650; y1 = 183; x2 = 745; y2 = 203;
        		break;
        	case 50:
        		x1 = 650; y1 = 217; x2 = 745; y2 = 237;
        		break;
        	case 51:
        		x1 = 650; y1 = 251; x2 = 745; y2 = 271;
        		break;
        	case 52:
        		x1 = 650; y1 = 285; x2 = 745; y2 = 305;
        		break;
        	case 53:
        		x1 = 650; y1 = 317; x2 = 745; y2 = 337;
        		break;
        	case 54:
        		x1 = 621; y1 = 350; x2 = 641; y2 = 450;
        		break;
        	case 55:
        		x1 = 220; y1 = 820; x2 = 240; y2 = 920;
        		break;
        	case 56:
        		x1 = 252; y1 = 820; x2 = 272; y2 = 920;
        		break;
        	case 57:
        		x1 = 284; y1 = 820; x2 = 304; y2 = 920;
        		break;
        	case 58:
        		x1 = 316; y1 = 820; x2 = 336; y2 = 920;
        		break;
        	case 59:
        		x1 = 348; y1 = 820; x2 = 368; y2 = 920;
        		break;
        	case 60:
        		x1 = 380; y1 = 820; x2 = 400; y2 = 920;
        		break;
        	case 61:
        		x1 = 412; y1 = 820; x2 = 432; y2 = 920;
        		break;
        	case 62:
        		x1 = 444; y1 = 820; x2 = 464; y2 = 920;
        		break;
        	case 63:
        		x1 = 476; y1 = 820; x2 = 496; y2 = 920;
        		break;
        	case 64:
        		x1 = 508; y1 = 820; x2 = 528; y2 = 920;
        		break;
        	case 65:
        		x1 = 540; y1 = 820; x2 = 560; y2 = 920;
        		break;
        	case 66:
        		x1 = 572; y1 = 820; x2 = 592; y2 = 920;
        		break;
        	case 67:
        		x1 = 604; y1 = 820; x2 = 624; y2 = 920;
        		break;
        	case 68:
        		x1 = 220; y1 = 950; x2 = 240; y2 = 1050;
        		break;
        	case 69:
        		x1 = 252; y1 = 950; x2 = 272; y2 = 1050;
        		break;
        	case 70:
        		x1 = 284; y1 = 950; x2 = 304; y2 = 1050;
        		break;
        	case 71:
        		x1 = 316; y1 = 950; x2 = 336; y2 = 1050;
        		break;
        	case 72:
        		x1 = 348; y1 = 950; x2 = 368; y2 = 1050;
        		break;
        	case 73:
        		x1 = 380; y1 = 950; x2 = 400; y2 = 1050;
        		break;
        	case 74:
        		x1 = 412; y1 = 950; x2 = 432; y2 = 1050;
        		break;
        	case 75:
        		x1 = 444; y1 = 950; x2 = 464; y2 = 1050;
        		break;
        	case 76:
        		x1 = 476; y1 = 950; x2 = 496; y2 = 1050;
        		break;
        	case 77:
        		x1 = 508; y1 = 950; x2 = 528; y2 = 1050;
        		break;
        	case 78:
        		x1 = 540; y1 = 950; x2 = 560; y2 = 1050;
        		break;
        	case 79:
        		x1 = 572; y1 = 950; x2 = 592; y2 = 1050;
        		break;
        	case 80:
        		x1 = 604; y1 = 950; x2 = 624; y2 = 1050;
        		break;
        	case 81:
        		x1 = 50; y1 = 860; x2 = 145; y2 = 1136;
        		break;
        	default:
        		x1 = 0; y1 = 0; x2 = 0; y2 = 0;
        		break;
        }
        tempCanvas2.drawRoundRect(new RectF(x1,y1,x2,y2), 2, 2, myPaint);
        tempCanvas.drawBitmap(tempBitmap2, 0, 0, null);
        tempCanvas.drawBitmap(tempBitmap3, 0, 0, null);
        myImageView.setImageBitmap(tempBitmap);
      }
    }
    
    public String accessDatabase(String... inputStrings) 
    {
    	String result = "",resultStr = "";
 
    	//the year data to send
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        
          nameValuePairs.add(new BasicNameValuePair("bookID", ""));
          nameValuePairs.add(new BasicNameValuePair("bookName", ""));   
          nameValuePairs.add(new BasicNameValuePair("bookGrade", "0"));   
          
         
          if(inputStrings[0].equalsIgnoreCase(Search)) {
        	nameValuePairs.add(new BasicNameValuePair("operation", Search));  
            nameValuePairs.add(new BasicNameValuePair("bookName", inputStrings[1]));
          }
          if(inputStrings[0].equalsIgnoreCase(List))
        	nameValuePairs.add(new BasicNameValuePair("operation", List));  
          
        InputStream is = null;
    	     
    	//http post
    	try {
    	  HttpClient httpclient = new DefaultHttpClient();
    	  HttpPost httppost = new HttpPost("http://library.csie.chu.edu.tw/library2.php");//遠端
    	  //HttpPost httppost = new HttpPost("http://192.168.2.105/app/library2.php");//自架serve
    	  httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    	  HttpResponse response = httpclient.execute(httppost);
    	  HttpEntity entity = response.getEntity();
    	  is = entity.getContent();
    	} catch(Exception e){
    	  Log.e("log_tag", "Error in http connection "+e.toString());
    	}
    	//convert response to string
    	try //PHP送回字串
    	{
    	  BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
    	  StringBuilder sb = new StringBuilder();
    	  String line = null;
    	  while ((line = reader.readLine()) != null) 
    	  {
    	    sb.append(line + "\n");
    	  }
    	  is.close();     
    	  result = sb.toString();	  
    	} 
    	catch(Exception e)
    	{
    	  Log.e("log_tag", "Error converting result " + e.toString());
    	}
    	     
    	//parse json data
    	try    	
    	{
    			JSONObject json_data = new JSONObject(result);//抓書櫃
    			resultStr= json_data.getString("number") + " , ";
    			JSONObject json_data_sum = new JSONObject(result);//抓個數
    			resultStr += json_data_sum.getString("bookGrade");
    		/*JSONArray jArray = new JSONArray(result);//抓列表
    		for (int i = 0; i < jArray.length(); i++) 
      		{
      			JSONObject json_data = jArray.getJSONObject(i);
      			resultStr += json_data.getString("bookID")+
      					", " + json_data.getString("bookName") +
      					", " + json_data.getDouble("bookGrade") + "\n";
      		}*/
    	}	
    	catch(JSONException e)
    	{
    		Log.e("log_tag", "Error parsing data "+e.toString());
    	}	
    	return resultStr;
    }
    
    public int SqlQuery(String sql) {	
    	Cursor c = db.rawQuery(sql, null);
    	c.moveToFirst();  // 第1筆	
    	EditText editText1 = (EditText) findViewById(R.id.edt_bookName);
    	int number = 0;
    	for (int i = 0; i < c.getCount(); i++) {
    		if(editText1.getText().toString().equals(c.getString(0))){
    			number = Integer.parseInt(c.getString(2));
    		}
    		c.moveToNext();  // 下一筆
       }
	return number;
    }
    
    public void call_app(View v)
    {   	 	
    	index %= 2;
    	
    	settings.edit().putString(like[index], item.getText().toString()).commit();
    	
   	 	try
	 	{
	 	  Intent intent = new Intent(ACTION_SCAN);  
		  intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); //QR code模式  
		  startActivityForResult(intent, 0);  
	 	}
	 	catch(ActivityNotFoundException anfe) 
	 	{
	 		Toast.makeText(v.getContext(), "Error", Toast.LENGTH_SHORT).show();
	 	}
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{  
		  if (requestCode == 0) 
		  {  
			  if (resultCode == RESULT_OK) 
			  {  
				  String contents = intent.getStringExtra("SCAN_RESULT"); //掃描結果  
				  String format = intent.getStringExtra("SCAN_RESULT_FORMAT");//掃描格式
				  
				  Toast your_like = Toast.makeText(this, "這是你喜歡的類別:"+ settings.getString(like[index], ""), Toast.LENGTH_LONG);
				  
				  int showarea = Integer.parseInt(contents);
				  
				  tempCanvas = new Canvas(tempBitmap);
			      tempCanvas.drawBitmap(myBitmap, 0, 0, null);
			        
			      tempBitmap3.eraseColor(0x00000000);
			      
				  switch(showarea)
				  {
				  	case 1:
				  		if(settings.getString(like[(index + 1) % 2], "") == "")
			  			{
			  				your_like.show();
			  			}
			  			if(settings.getString(like[index], "").equalsIgnoreCase(settings.getString(like[(index + 1) % 2], "")))
			  			{
			  				your_like.show();
			  			}
				  		x1 = 340; y1 = 90; x2 = 500; y2 = 200;
				  		break;
				  	case 2:
				  		x1 = 200; y1 = 220; x2 = 620; y2 = 330;
				  		break;
				  	case 3:
				  		x1 = 200; y1 = 350; x2 = 650; y2 = 450;
				  		break;
				  	case 4:
				  		x1 = 220; y1 = 820; x2 = 620; y2 = 920;
				  		break;
				  	case 5:
				  		x1 = 220; y1 = 950; x2 = 620; y2 = 1050;
				  		break;
				  	case 6:
				  		x1 = 55; y1 = 130; x2 = 150; y2 = 475;
				  		break;
				  	case 7:
				  		x1 = 650; y1 = 115; x2 = 745; y2 = 337;
				  		break;
				  	case 8:
				  		x1 = 50; y1 = 860; x2 = 145; y2 = 1136;
				  		break;
				  	default:
				  		break;
				  }
				  myPaint2.setStyle(Style.STROKE);
			      myPaint2.setStrokeWidth(10);
			      tempCanvas3.drawRoundRect(new RectF(x1,y1,x2,y2), 2, 2, myPaint2);
			      tempCanvas.drawBitmap(tempBitmap2, 0, 0, null);
			      tempCanvas.drawBitmap(tempBitmap3, 0, 0, null);
			      myImageView.setImageBitmap(tempBitmap);
			  }
		  }
		  index++;
	} 
}
