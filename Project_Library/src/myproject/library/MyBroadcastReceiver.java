package myproject.library;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyBroadcastReceiver extends BroadcastReceiver {
	
	private NotificationManager gNotMgr = null;

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		gNotMgr = (NotificationManager) context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
		// TODO Auto-generated method stub
		
		int id = 1;
		
		//currentTimeMillis 這是要讓通知馬上出現
		Notification notification = new Notification(R.drawable.ic_launcher, "", System.currentTimeMillis());
		
		long[] tVibrate = {0,100,200,300};
		notification.vibrate = tVibrate;
		
		//這是讓PendingIntent知道要啟動哪個activity
		Intent notificationIntent = new Intent(context, cover.class);
		
		//這是用來啟動activity
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		
		notification.setLatestEventInfo(context, "有新書來囉", "快去看看吧", contentIntent);
		gNotMgr.notify(id++, notification);
	}

}
